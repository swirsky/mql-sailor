//+------------------------------------------------------------------+
//|                                                       Sailor.mq4 |
//|                                 Copyright 2016, Łukasz Swierszcz |
//|                                          http://solutionists.pl/ |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, Łukasz Swierszcz"
#property link      "http://solutionists.pl/"
#property version   "1.00"
#property strict
//--- input parameters
input long     magic=0;
input int      trailingFromPosition=1;
input int      trailingDistance=20;
//+------------------------------------------------------------------+
enum SORT_TYPES
  {
   SORT_TIMESTAMP_ASC=1,
   SORT_TIMESTAMP_DESC=2,
   SORT_PROFIT_ASC=4,
   SORT_PROFIT_DESC=8

  };
//+------------------------------------------------------------------+
struct order
  {
   int               ticket;
   int               type;
   int               profit;
   datetime          timestamp;
   double            openPrice;
   double            stopLoss;
  };

order orders[];

int ordersCount=0;
int openedOrdersCount=0;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
   if(trailingFromPosition<=0)
      return(INIT_PARAMETERS_INCORRECT);

// Collect current orders
   openedOrdersCount=0;
   ordersCount=GetOrders(orders);

   for(int i=0;i<ordersCount;i++)
     {
      if(orders[i].type==OP_BUY || orders[i].type==OP_SELL)
         ++openedOrdersCount;
     }

   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
// Collect current positions
   int currentOrdersCount=GetOrders(orders);
   int currentOpenedOrdersCount=0;
   order openedOrders[];

// Filter opened orders
   for(int i=0;i<currentOrdersCount;i++)
     {
      if(orders[i].type==OP_BUY || orders[i].type==OP_SELL)
        {
         ArrayResize(openedOrders,currentOpenedOrdersCount+1);
         openedOrders[currentOpenedOrdersCount]=orders[i];
         ++currentOpenedOrdersCount;
        }
     }

   Comment(openedOrdersCount," ",currentOpenedOrdersCount);

   if(currentOpenedOrdersCount>0)
     {
      // Close all orders if count is smaller than in previous iteration
      if(currentOpenedOrdersCount<openedOrdersCount)
        {
         for(int i=0;i<currentOpenedOrdersCount;i++)
           {
            if(OrderSelect(openedOrders[i].ticket,SELECT_BY_TICKET))
              {
               if(!OrderDelete(openedOrders[i].ticket))
                 {
                  Print("Failed to delete order: ",GetLastError());
                 }
              }
           }
        }

      // Processing opened orders

      //SortOrders(openedOrders,SORT_PROFIT_ASC);
      for(int i=0;i<currentOpenedOrdersCount;i++)
        {
         //---
         //Print(openedOrders[i].ticket," ",openedOrders[i].profit);
         if(!OrderSelect(openedOrders[i].ticket,SELECT_BY_TICKET))
            continue;

         // Add stop loss to existing orders if not exists
         //Print(i," : ",openedOrders[i].ticket," = ",openedOrders[i].stopLoss);
         if(!openedOrders[i].stopLoss)
           {
            double sl=(OrderType()==OP_SELL ? Ask+trailingDistance*Point : Bid-trailingDistance*Point);

            if(!OrderModify(OrderTicket(),OrderOpenPrice(),sl,OrderTakeProfit(),0))
               Print("Failed to initialize SL: ",GetLastError());
           }

         // Trailing stop
         if(currentOpenedOrdersCount>trailingFromPosition && openedOrders[trailingFromPosition].profit>trailingDistance)
           {
            if(openedOrders[i].type==OP_BUY)
              {
               double sl=NormalizeDouble(Bid-trailingDistance*Point,Digits());
               if(openedOrders[i].stopLoss<sl)
                  if(!OrderModify(OrderTicket(),OrderOpenPrice(),sl,OrderTakeProfit(),0))
                     Print("Cannot move SL: ",GetLastError());
              }
            else if(openedOrders[i].type==OP_SELL)
              {
               double sl=NormalizeDouble(Ask+trailingDistance*Point,Digits());
               if(openedOrders[i].stopLoss>sl)
                  if(!OrderModify(OrderTicket(),OrderOpenPrice(),sl,OrderTakeProfit(),0))
                     Print("Cannot move SL: ",GetLastError());
              }
           }
        }
     }

// Update global variables
   ordersCount=currentOrdersCount;
   openedOrdersCount=currentOpenedOrdersCount;
  }
//+------------------------------------------------------------------+
//| Prepare orders struct array                                      |
//+------------------------------------------------------------------+
int GetOrders(order &items[])
  {
   ArrayResize(items,0);
   int count=0;
// Collect current positions
   if(OrdersTotal()>0)
     {
      for(int i=0; i<OrdersTotal(); i++)
        {
         if(!OrderSelect(i,SELECT_BY_POS))
            continue;

         if(OrderSymbol()==Symbol() && (magic==0 || (magic!=0 && magic==OrderMagicNumber())))
           {
            order position={0};
            position.ticket=OrderTicket();
            position.type=OrderType();
            position.profit=MathRound((OrderProfit()-OrderCommission())/OrderLots()/MarketInfo(OrderSymbol(),MODE_TICKVALUE));
            position.timestamp=OrderOpenTime();
            position.openPrice=OrderOpenPrice();
            position.stopLoss=OrderStopLoss();

            int size=ArraySize(items);
            ArrayResize(items,size+1);
            items[size]=position;
            count=size;
           }
        }
     }
   return count;
  }
//+------------------------------------------------------------------+
//| Sort orders                                                      |
//+------------------------------------------------------------------+
void SortOrders(order &items[],SORT_TYPES mode=SORT_PROFIT_DESC)
  {
   int size=ArraySize(items);
   for(int i=0;i<size;i++)
     {
      for(int j=0;j<size;j++)
        {
         switch(mode)
           {
            case SORT_TIMESTAMP_ASC:
               if(items[i].timestamp>items[j].timestamp) OrderShuffle(items,i,j);
               break;
            case SORT_TIMESTAMP_DESC:
               if(items[i].timestamp<items[j].timestamp) OrderShuffle(items,i,j);
               break;
            case SORT_PROFIT_ASC:
               if(items[i].profit>items[j].profit) OrderShuffle(items,i,j);
               break;
            case SORT_PROFIT_DESC:
               if(items[i].profit<items[j].profit) OrderShuffle(items,i,j);
               break;
           }
        }
     }
  }
//+------------------------------------------------------------------+
//| Swaps elements in orders array                                   |
//+------------------------------------------------------------------+
bool OrderShuffle(order &items[],int x,int y)
  {
   int size=ArraySize(items);
   if(x>=0 || x<size || y>=0 || y<size)
     {
      order temp=items[x];
      items[x] = items[y];
      items[y] = items[x];
      return true;
     }
   return false;
  }
//+------------------------------------------------------------------+
