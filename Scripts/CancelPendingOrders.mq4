//+------------------------------------------------------------------+
//|                                          CancelPendingOrders.mq4 |
//|                                 Copyright 2016, Łukasz Swierszcz |
//|                         https://bitbucket.org/swirsky/mql-sailor |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, Łukasz Swierszcz"
#property link      "https://bitbucket.org/swirsky/mql-sailor"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
//---
   for(int i=OrdersTotal()-1; i>=0; i--)
     {
      if(!OrderSelect(i,SELECT_BY_POS))
         continue;

      if(OrderSymbol()==Symbol() && (OrderType()!=OP_BUY || OrderType()!=OP_BUY))
        {
         if(!OrderDelete(OrderTicket()))
           {
            Print("Failed to delete order: ",GetLastError());
           }
        }
     }
  }
//+------------------------------------------------------------------+
