//+------------------------------------------------------------------+
//|                                                    ShortGrid.mq4 |
//|                                 Copyright 2016, Łukasz Swierszcz |
//|                         https://bitbucket.org/swirsky/mql-sailor |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, Łukasz Swierszcz"
#property link      "https://bitbucket.org/swirsky/mql-sailor"
#property version   "1.00"
#property strict
//--- parameters
int      positions=4;        // Number of positions for each side
int      initialDistance=1;  // Distance from current price (points)
int      gap=10;             // Gap between orders (points)
int      sl=0;               // Initial SL (points)
double   volume=0.1;         // Postion size (lots)
int      slippage=0;
int      magic=0;
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
void OnStart()
  {
   double stopLoss=0;

   if(sl)
     {
      stopLoss=Bid-sl/MathPow(10,Digits);
     }

   for(int i=0;i<=positions;i++)
     {
      double price=Bid-
                   initialDistance/MathPow(10,Digits)-
                   gap*i/MathPow(10,Digits);

      if(!OrderSend(Symbol(),OP_SELLSTOP,volume,price,slippage,stopLoss,0,"Short grid "+IntegerToString(i),magic))
        {
         Print("OrderSend failed with error: ",GetLastError());
        }
     }
  }
//+------------------------------------------------------------------+
